import Vue from 'vue'
import VueLazyLoad from 'vue-lazyload'

Vue.use(VueLazyLoad,{
  preLoad: 1.3,
  error:'statics/img/jacket.png',
  loading:'statics/img/jacket.png',
  attempt: 1,
  lazyComponent: true
})
