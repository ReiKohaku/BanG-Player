export default {
  public: {
    notChrome: 'Chromeブラウザを使ってこのサイトにアクセスすることをお勧めします。',
    ok: 'OK',
    cancel: 'キャンセル',
    success: '成功',
    failed: '失敗',
    error: 'エラー',
    selectImage: '画像を選択...',
    reset: 'リセット'
  },
  data: {
    language: {
      en_us: 'English',
      zh_cn: '中文（简体）',
      ja_jp: '日本語'
    },
    skin: {
      skin: 'スタイルの設定に従う',
      skin00: 'デフォルト',
      skin01: 'スタイル2',
      skin02: 'スタイル3',
      skin03: 'スタイル4',
      skin04: 'スタイル5',
      challenge: 'チャレンジライブ',
      vs: '対バンライブ',
      persona: 'ペルソナ persona',
      miku: '初音ミク',
      cafe: 'ご注文はうさぎですか？',
      maid: 'Re：ゼロから始める異世界生活',
      gbp2020: 'ガルパ2020',
      april_fool: 'エイプリルフール',
      coin: 'とある科学の超電磁砲T',
      cover: '楽曲カバー',
      black: 'ブラック',
      custom: 'カスタム'
    },
    proxy: {
      direct: 'Bestdori!に直接接続（デフォルト）',
      cn1: '中国-張家口（1Mbps，中国では遅いが安定している）',
      cn2: '中国-上海（1Mbps，中国では遅いが安定している）',
      cn3: '中国-上海（5Mbps，中国では遅いが安定している）',
      fr1: 'フランス（デュプレクス100 Mbps）'
    }
  },
  notification: {
    title: 'お知らせ',
    read: '既読としてマーク',
    readAll: 'すべて既読にする',
    isRead: '読み終わった'
  },
  about: {
    title: '関連情報'
  },
  share: {
    title: '共有',
    tip: 'このリンクを手動でコピーして共有できます。'
  },
  option: {
    title: '設定',
    reset: {
      label: 'リセット'
    },
    live: {
      title: 'ライブ設定',
      speed: 'ノーツのスピード',
      noteScale: 'ノーツのサイズ',
      judgeOffset: '判定調節',
      visualOffset: 'オフセット調節',
      barOpacity: '長押しラインの濃さ',
      backgroundDim: '背景画像の透明度',
      resolution: '解像度',
      showSimLine: '同時押しライン',
      beatNote: 'リズムサポート',
      mirror: 'ミラー',
      laneEffect: 'レーンエフェクト',
      autoFullscreen: '自動フルスクリーン',
      upperHidden: '上を隠す',
      upperHiddenImage: '上を隠す画像'
    },
    effect: {
      title: 'ライブスキン·音量設定',
      skin: 'ライブスキン',
      sound: 'ライブSE',
      perfect: 'タップ音',
      flick: 'フリック音',
      bg: '背景',
      customBg: '背景画像をカスタマイズ',
      effectVolume: 'ライブSE音量'
    },
    locale: {
      title: '言語·ネットワーク設定',
      language: '言語',
      proxy: 'リソースサーバ',
      testDone: '最高速度 {max}，平均速度 {avg}'
    },
    OPTION_RESET: 'リセットしました。'
  },
  favorite: {
    title: 'お気に入り',
    empty: '何もないです……',
    tip: 'お気に入りは左にスワイプすると消去できます',
    FAVORITE_ADDED: '{title} をお気に入りに登録しました。',
    FAVORITE_ADD_FAILED: '{title} をお気に入りに登録できませんでした。',
    FAVORITE_REMOVED: '{title} をお気に入りから削除しました。',
    FAVORITE_REMOVE_FAILED: 'お気に入りから {title} を削除できませんでした。',
    FAVORITE_CLEAR: 'お気に入りが空です。'
  },
  find: {
    title: '発見',
    hot: '人気がある',
    lastRefresh: '最終更新は {time}',
    latest: '最新',
    nothing: '何もありません……',
    loading: '読み込み中……'
  },
  officialSongList: {
    title: '本家譜面リスト',
    loading: '頑張ってロード中ですQwQ'
  },
  communityChartSearch: {
    title: '検索'
  },
  mod: {
    title: 'Mod',
    none: {
      name: 'なし',
      caption: 'Modを使用しない。'
    },
    flickParty: {
      name: 'Flick Party!',
      caption: 'タップとスライド端をすべてフリックに変更します。'
    },
    singleDog: {
      name: 'Single Dog',
      caption: 'すべてのノートをタップに変更します。'
    },
    allFlick: {
      name: 'All Flick',
      caption: 'タップやスライド端だけでなく……'
    }
  },
  play: {
    load: {
      label: '譜面をロードする',
      gettingChartInfo: '譜面情報取得中……',
      gettingBandInfo: 'バンド情報取得中……',
      gettingChartData: '譜面データ取得中……',
      resolvingData: '解析データ中……'
    },
    start: {
      label: 'プレイ'
    },
    chartSource: {
      label: '譜面のソース',
      bestdori: {
        label: 'Bestdori!',
        chartId: '譜面ID',
        type: {
          label: '譜面のタイプ',
          official: {
            label: '本家',
            hint: '本家譜面のIDは詳細ページにあります。右側のボタンをクリックして検索を開始することもできます。'
          },
          community: {
            label: 'オリジナル',
            hint: 'スペクトル面の詳細URLのうち、「chart\\」の後ろの数字が譜面IDです。'
          }
        }
      },
      local: {
        label: 'ローカル',
        type: {
          label: '譜面のタイプ',
          bangpack: {
            label: 'BanG pack',
            hint: '.bangpackで終わるカバン。右下のダウンロードボタンで入手できます。',
            select: 'BanG packを選択……'
          },
          bangbangboomv2: {
            label: 'bangbangboom v2',
            select: 'ファイルからbangbangboom v2譜面を読み取る……'
          },
          bestdori: {
            label: 'Bestdori!',
            select: 'ファイルからBestdori!譜面を読み取る……'
          },
          select: {
            audio: {
              label: 'ミュージックファイル'
            },
            cover: {
              label: '譜面カバー'
            },
            songName: {
              label: '曲名'
            }
          },
          start: 'プレイ'
        }
      }
    }
  },
  INFO_COMING_SOON: 'カミングスーン……',
  INFO_COPIED: 'クリップボードにコピーしました。',
  INFO_COPY_FAILED: 'クリップボードにリンクしたコピーに失敗しました。',
  INFO_CONVERTING_CHART: '譜面変換中……',
  INFO_GETTING_DATA: '情報取得中……',
  INFO_PACKING: '圧縮中……',
  INFO_LOADING_BANGPACK: '読み込み中……',
  ERR_SAVE_OPTIONS_FAILED: '設定を保存中に未知のエラーが発生しました。',
  ERR_INVALID_ARGUMENT: 'パラメータが無効です。確認してください。',
  ERR_NOT_A_CHART: 'これは譜面ではない。',
  ERR_WRONG_CHART_ID: 'この譜面IDは正しくないです。',
  ERR_NO_CHART_INFO: 'まだ譜面をロードしていません。先にロードしてください。',
  ERR_LOAD_FILE_ERROR: 'ファイルの読み込み中にエラーが発生しました。',
  ERR_GET_DATA_FAILED: 'データの取得に失敗しました。ネットワーク接続を確認してください。',
  ERR_FULLSCREEN: '自動フルスクリーンが失敗しました……',
  'Network Error': 'データの取得に失敗しました。ネットワーク接続を確認してください。',
  LOCK: 'すみません、サポートされていないブラウザを使っているので、あなたのアクセスは停止されました。Chromeブラウザを使ってアクセスすることをお勧めします。'
}
