export default {
  public: {
    notChrome: '建议您使用Chrome浏览器访问此网站。',
    ok: '确认',
    cancel: '取消',
    success: '成功',
    failed: '失败',
    error: '错误',
    selectImage: '选择图片……',
    reset: '重置'
  },
  data: {
    language: {
      en_us: 'English',
      zh_cn: '中文（简体）',
      ja_jp: '日本語'
    },
    skin: {
      skin: '跟随皮肤',
      skin00: '默认',
      skin01: '皮肤2',
      skin02: '皮肤3',
      skin03: '皮肤4',
      skin04: '皮肤5',
      challenge: '挑战演出',
      vs: '竞演演出',
      persona: '女神异闻录Persona',
      miku: '初音未来',
      cafe: '请问您今天要来点兔子吗？',
      maid: 'Re：从零开始的异世界生活',
      gbp2020: '少女乐团派对2020',
      april_fool: '愚人节',
      coin: '某科学的超电磁炮T',
      cover: '使用乐曲封面',
      black: '纯黑',
      custom: '自定义背景'
    },
    proxy: {
      direct: '直连Bestdori!（默认）',
      cn1: '中国-张家口（1Mbps，国内稳定）',
      cn2: '中国-上海（1Mbps，国内稳定）',
      cn3: '中国-上海（5Mbps，国内稳定）',
      fr1: '法国（双工100Mbps）'
    }
  },
  notification: {
    title: '通知',
    read: '标记为已读',
    readAll: '标记全部为已读',
    isRead: '已读'
  },
  about: {
    title: '关于'
  },
  share: {
    title: '分享',
    tip: '你可以手动复制这个链接来分享！'
  },
  option: {
    title: '设置',
    reset: {
      label: '重置'
    },
    live: {
      title: '演出设定',
      speed: '节奏图标的速度',
      noteScale: '节奏图标的大小',
      judgeOffset: '判定偏移',
      visualOffset: '视觉偏移',
      barOpacity: '长按按键条浓度',
      backgroundDim: '背景图片亮度',
      resolution: '分辨率',
      showSimLine: '同时点击线',
      beatNote: '节奏的色觉辅助',
      mirror: '镜像',
      laneEffect: '判定辅助效果',
      autoFullscreen: '自动全屏',
      upperHidden: '上隐板',
      upperHiddenImage: '上隐板图片'
    },
    effect: {
      title: '皮肤·音量设定',
      skin: '皮肤',
      sound: '音效',
      perfect: 'Tap音',
      flick: 'Flick音',
      bg: '背景',
      customBg: '自定义背景图片',
      effectVolume: '击打效果音量'
    },
    locale: {
      title: '语言·网络设定',
      language: '语言',
      proxy: '资源服务器',
      testDone: '最高速度 {max}，平均速度 {avg}'
    },
    OPTION_RESET: '设置已重置。'
  },
  favorite: {
    title: '收藏',
    empty: '空空如也呀QAQ',
    tip: '左滑可以删除收藏的项目',
    FAVORITE_ADDED: '成功加入 {title}',
    FAVORITE_ADD_FAILED: '加入 {title} 失败',
    FAVORITE_REMOVED: '成功移除 {title}',
    FAVORITE_REMOVE_FAILED: '移除 {title} 失败',
    FAVORITE_CLEAR: '已清空收藏。'
  },
  find: {
    title: '发现',
    hot: '最热',
    lastRefresh: '最后更新于 {time}',
    latest: '最新',
    nothing: '什么也没有呀QAQ',
    loading: '加载中……'
  },
  officialSongList: {
    title: '官谱列表',
    loading: '在很努力地加载啦QwQ'
  },
  communityChartSearch: {
    title: '搜索'
  },
  mod: {
    title: 'Mod',
    none: {
      name: '无',
      caption: '不使用Mod。'
    },
    flickParty: {
      name: 'Flick Party!',
      caption: '把单键和滑条尾都变成滑键！'
    },
    singleDog: {
      name: 'Single Dog',
      caption: '大家都是单身狗XD'
    },
    allFlick: {
      name: 'All Flick',
      caption: '不止是单键和滑条尾……'
    }
  },
  play: {
    load: {
      label: '加载',
      gettingChartInfo: '获取谱面信息……',
      gettingBandInfo: '获取乐队信息……',
      gettingChartData: '获取谱面数据……',
      resolvingData: '解析数据……'
    },
    start: {
      label: '开始'
    },
    chartSource: {
      label: '谱面来源',
      bestdori: {
        label: 'Bestdori!',
        chartId: '谱面ID',
        type: {
          label: '谱面类型',
          official: {
            label: '官方',
            hint: '官谱的ID在歌曲详情页面里，或者您可以点击右侧的按钮查找。'
          },
          community: {
            label: '社区',
            hint: '自制谱页面的URL中，chart\\ 后面的一串数字即为ID。'
          }
        }
      },
      local: {
        label: '本地',
        type: {
          label: '加载类型',
          bangpack: {
            label: 'BanG pack',
            hint: '以.bangpack结尾的包。你可以通过右下角的下载按钮获取。',
            select: '选择一个BanG pack……'
          },
          bangbangboomv2: {
            label: 'bangbangboom v2',
            select: '从文件读取bangbangboom v2谱面……'
          },
          bestdori: {
            label: 'Bestdori!',
            select: '从文件读取Bestdori!谱面……'
          },
          select: {
            audio: {
              label: '音乐'
            },
            cover: {
              label: '封面'
            },
            songName: {
              label: '音乐名'
            }
          },
          start: '开始'
        }
      }
    }
  },
  INFO_COMING_SOON: '在做了在做了~',
  INFO_COPIED: '已复制链接。',
  INFO_COPY_FAILED: '复制链接失败……',
  INFO_CONVERTING_CHART: '正在转换谱面……',
  INFO_GETTING_DATA: '正在获取信息，请稍候……',
  INFO_PACKING: '打包中，请稍候……',
  INFO_LOADING_BANGPACK: '正在加载BanG pack，请稍候……',
  ERR_SAVE_OPTIONS_FAILED: '保存设置时发生了未知的错误。',
  ERR_INVALID_ARGUMENT: '无效的参数，请检查。',
  ERR_NOT_A_CHART: '这不是一张谱面。',
  ERR_WRONG_CHART_ID: '这个谱面ID的值不正确。',
  ERR_NO_CHART_INFO: '你还没有加载谱面，请先加载。',
  ERR_LOAD_FILE_ERROR: '加载文件时发生错误。',
  ERR_GET_DATA_FAILED: '获取数据失败，请检查您的网络环境',
  ERR_FULLSCREEN: '自动启动全屏失败了……',
  'Network Error': '网络错误。',
  LOCK: '抱歉，您的访问已被阻止，因为您使用了不受支持的浏览器。建议您使用Chrome浏览器进行访问。'
}
