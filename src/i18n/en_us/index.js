export default {
  public: {
    notChrome: 'It is recommended to use Chrome browser to visit this website.',
    ok: 'OK',
    cancel: 'Cancel',
    success: 'Success',
    failed: 'Failed',
    error: 'Error',
    selectImage: 'Select Image...',
    reset: 'Reset'
  },
  data: {
    language: {
      en_us: 'English',
      zh_cn: '中文（简体）',
      ja_jp: '日本語'
    },
    skin: {
      skin: 'Change with skin',
      skin00: 'Default',
      skin01: 'Skin2',
      skin02: 'Skin3',
      skin03: 'Skin4',
      skin04: 'Skin5',
      challenge: 'Challenge Live',
      vs: 'VS Live',
      persona: 'Revelations: Persona',
      miku: 'Hatsune Miku',
      cafe: 'Would you like some rabbits today?',
      maid: 'Re: Life in a different world from zero',
      gbp2020: 'Garupa 2020',
      april_fool: 'April fool',
      coin: 'A Certain Scientific Railgun T',
      cover: 'Use song\'s cover',
      black: 'Black',
      custom: 'Choose a picture you like'
    },
    proxy: {
      direct: 'Direct to Bestdori! (Recommended)',
      cn1: 'China-Zhangjiakou (1Mbps, stable for Chinese users)',
      cn2: 'China-Shanghai (1Mbps, stable for Chinese users)',
      cn3: 'China-Shanghai (5Mbps, stable for Chinese users)',
      fr1: 'French-Eppes (Duplex 100Mbps)'
    }
  },
  notification: {
    title: 'Notification',
    read: 'Mark as read',
    readAll: 'Mark all as read',
    isRead: 'Read'
  },
  about: {
    title: 'About'
  },
  share: {
    title: 'Share',
    tip: 'You can copy this link by yourself to share!'
  },
  option: {
    title: 'Options',
    reset: {
      label: 'Reset'
    },
    live: {
      title: 'Live',
      speed: 'Note Speed',
      noteScale: 'Note Scale',
      judgeOffset: 'Judge Offset',
      visualOffset: 'Visual Offset',
      barOpacity: 'Long Note Transparency',
      backgroundDim: 'Background Dim',
      resolution: 'Resolution',
      showSimLine: 'Dual Tap Line',
      beatNote: 'Off-beat Coloring',
      mirror: 'Mirror',
      laneEffect: 'Lane Effect',
      autoFullscreen: 'Auto Fullscreen',
      upperHidden: 'Upper Hidden',
      upperHiddenImage: 'Upper Hidden Image'
    },
    effect: {
      title: 'Skin & Volume',
      skin: 'Skin',
      sound: 'Sound Effect',
      perfect: 'Perfect',
      flick: 'Flick',
      bg: 'Background',
      customBg: 'Custom Background Image',
      effectVolume: 'Effect Volume'
    },
    locale: {
      title: 'Language & Network',
      language: 'Language',
      proxy: 'Resource Server',
      testDone: 'Max speed {max}, average speed {avg}'
    },
    OPTION_RESET: 'Option reset.'
  },
  favorite: {
    title: 'Favorite',
    empty: 'Nothing here QAQ',
    tip: 'Left slide for remove the item.',
    FAVORITE_ADDED: 'Successfully added {title}',
    FAVORITE_ADD_FAILED: 'Failed to add {title}',
    FAVORITE_REMOVED: 'Successfully removed {title}.',
    FAVORITE_REMOVE_FAILED: 'Failed to remove {title}.',
    FAVORITE_CLEAR: 'Favorite List Clear.'
  },
  find: {
    title: 'Find',
    hot: 'Hot',
    lastRefresh: 'Last refresh at {time}',
    latest: 'Latest',
    nothing: 'Nothing here QAQ',
    loading: 'Loading...'
  },
  officialSongList: {
    title: 'Songs of \"BanG Dream! Girls Band Party!\"',
    loading: 'Loading, please wait for a moment...'
  },
  communityChartSearch: {
    title: 'Search'
  },
  mod: {
    title: 'Mod',
    none: {
      name: 'None',
      caption: 'Do not use mods.'
    },
    flickParty: {
      name: 'Flick Party!',
      caption: 'Every single and slide end note will be changed to flick!'
    },
    singleDog: {
      name: 'Single Dog',
      caption: 'You will be always single XD'
    },
    allFlick: {
      name: 'All Flick',
      caption: 'Not only single and slide end...'
    }
  },
  play: {
    load: {
      label: 'Load',
      gettingChartInfo: 'Getting chart info...',
      gettingBandInfo: 'Getting band info...',
      gettingChartData: 'Getting chart data...',
      resolvingData: 'Resolving data...'
    },
    start: {
      label: 'Start'
    },
    chartSource: {
      label: 'Chart Source',
      bestdori: {
        label: 'Bestdori!',
        chartId: 'Chart ID',
        type: {
          label: 'Chart Type',
          official: {
            label: 'Official',
            hint: 'ID of official chart is in the page of song, or you can find songs in the list.'
          },
          community: {
            label: 'Community',
            hint: 'The number after \"chart\\\" in the URL is ID of the community chart.'
          }
        }
      },
      local: {
        label: 'Local',
        type: {
          label: 'Load Type',
          bangpack: {
            label: 'BanG pack',
            hint: 'A pack which name is end with .bangpack. You can get it by using download button at right-bottom of the page.',
            select: 'Select a BanG Pack...'
          },
          bangbangboomv2: {
            label: 'bangbangboom v2',
            select: 'Load bangbangboom v2 chart from file...'
          },
          bestdori: {
            label: 'Bestdori!',
            select: 'Load Bestdori! chart from file...'
          },
          select: {
            audio: {
              label: 'Audio'
            },
            cover: {
              label: 'Cover'
            },
            songName: {
              label: 'Song name'
            }
          },
          start: 'Start'
        }
      }
    }
  },
  INFO_COMING_SOON: 'Coming soon!',
  INFO_COPIED: 'Link copied.',
  INFO_COPY_FAILED: 'Failed to copy the link.',
  INFO_CONVERTING_CHART: 'Converting chart...',
  INFO_GETTING_DATA: 'Getting data, please wait...',
  INFO_PACKING: 'Packing, please wait...',
  INFO_LOADING_BANGPACK: 'Loading BanG pack, please wait...',
  ERR_SAVE_OPTIONS_FAILED: 'An error occurred when saving the options.',
  ERR_INVALID_ARGUMENT: 'Invalid argument, please check.',
  ERR_NOT_A_CHART: 'This is not a chart.',
  ERR_WRONG_CHART_ID: 'This chart ID is not a valid value.',
  ERR_NO_CHART_INFO: 'You didn\'t load a chart, please load it first.',
  ERR_LOAD_FILE_ERROR: 'An error occurred when loading the file.',
  ERR_GET_DATA_FAILED: 'Get data failed, please check your network.',
  ERR_FULLSCREEN: 'Auto fullscreen failed this time...',
  'Network Error': 'Network Error.',
  LOCK: 'Sorry, but your visit has been refused because of you are using a unsupported browser. It is suggested using Chrome to visit this site.'
}
