import enUS from './en_us'
import zhCN from './zh_cn'
import jaJP from './ja_jp'

export default {
  'en_us': enUS,
  'zh_cn': zhCN,
  'ja_jp': jaJP
}
