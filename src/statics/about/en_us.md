Open Sourced on [gitee](https://gitee.com/ReiKohaku/BanG-Player).

Website made by @ReiKohaku ([github](https://github.com/reikohaku) | [gitee](https://gitee.com/ReiKohaku) | [bilibili](https://space.bilibili.com/88633132)).

Simulator using **bangbangboom-game** ([github](https://github.com/K024/bangbangboom-game) | [gitee](https://gitee.com/ReiKohaku/bangbangboom-game)).

Special thanks to **[Bestdori!](https://bestdori.com)**.

Any suggestions or questions, please contact the author, or give an issue of the project.