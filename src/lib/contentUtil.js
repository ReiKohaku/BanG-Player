export function bestdori(bestdoriChart) {
  let timepoint = [];
  let notes = [];
  let slides = [];
  let posA = -1;
  let posB = -1;
  let long = [-1, -1, -1, -1, -1, -1, -1];

  bestdoriChart.forEach(item => {
    switch (item.type) {
      case 'System':
        if (item.cmd === 'BPM') timepoint.push(item);
        break;
      case 'Note':
        let note = {
          time: bestdoriGetNoteTime(timepoint, item.beat),
          lane: item.lane - 1,
          onbeat: item.beat % 0.5 === 0
        };
        if (item.note === 'Slide') {
          note.type = 'slide';
          if (item.start) {
            let slide = {id: slides.length, flickend: false};
            (item.pos === 'A') ? (posA = slide.id) : (posB = slide.id);
            slides.push(slide);
            note.slideid = slide.id;
          } else if (item.end) {
            note.slideid = (item.pos === 'A') ? posA : posB;
            if (note.slideid === -1) {
              //因为有的谱面有很奇怪的无长度绿条，所以加一个判断
              note.type = item.flick ? 'flick' : 'single';
              break;
            }
            if (item.flick) slides[note.slideid].flickend = true;
            (item.pos === 'A') ? (posA = -1) : (posB = -1);
          } else note.slideid = (item.pos === 'A') ? posA : posB;
        } else if (item.note === 'Long') {
          note.type = 'slide';
          if (item.start) {
            let slide = {id: slides.length, flickend: false};
            long[note.lane] = slide.id;
            note.slideid = slide.id;
            slides.push(slide);
          } else if (item.end) {
            note.slideid = long[note.lane];
            if (item.flick) slides[note.slideid].flickend = true;
            long[note.lane] = -1
          } else note.slideid = long[note.lane]
        } else {
          note.type = item.flick ? 'flick' : 'single';
        }
        notes.push(note);
        break;
      default:
        break;
    }
  })
  return {notes, slides};
}

function bestdoriGetNoteTime(timepoints, beat) {
  let time = 0;
  for (let i = 0; i < timepoints.length; i++) {
    if (i === timepoints.length - 1)
      time += (beat - timepoints[i].beat) * 60 / timepoints[i].bpm;
    else if (beat > timepoints[i + 1].beat)
      time += (timepoints[i + 1].beat - timepoints[i].beat) * 60 / timepoints[i].bpm;
    else
      time += (beat - timepoints[i].beat) * 60 / timepoints[i].bpm;
  }
  return time;
}

export function bangbangboomv2(bangbangboomv2Chart) {
  //使用bangbangboom-editor中的源码重写
  let notes = [];
  let slides = [];
  let noteSlideIndex = {};

  bangbangboomv2Chart.slides.forEach(s => {
    noteSlideIndex[s.id] = slides.length;
    slides.push({ id: slides.length, flickend: s.flickend });
  })

  bangbangboomv2Chart.notes.forEach(n => {
    let timepoint;
    for(let i in bangbangboomv2Chart.timepoints)
      if(bangbangboomv2Chart.timepoints[i].id === n.timepoint)
        timepoint = bangbangboomv2Chart.timepoints[i];
    let note = {
      time: bbbv2ComputeTime(timepoint.time,
        { bpm: timepoint.bpm, beat: 0},
        { beat: n.offset / 48}),
      lane: n.lane,
      onbeat: (n.offset % 24 === 0)
    };
    if(n.type === 'single') note.type = 'single';
    else if (n.type === 'flick') note.type = 'flick';
    else {
      note.type = 'slide';
      note.slideid = noteSlideIndex[n.slide];
    }
    notes.push(note);
  })
  return { notes, slides };
}

function bbbv2ComputeTime(lastTime, lastObject, thisObject) {
  let time = lastTime + 60 / lastObject.bpm * (thisObject.beat - lastObject.beat);
  return !time ? 0 : time
}
