import {LocalStorage} from 'quasar';
import {Quasar} from 'quasar';

const languageList = ['en_us', 'zh_cn', 'ja_jp'];
const skinList = ['skin00', 'skin04', 'cafe', 'maid', 'april_fool', 'coin'];
const soundList = ['skin', 'skin00', 'skin01', 'skin02', 'skin03', 'persona', 'cafe', 'miku', 'maid', 'april_fool', 'coin'];
const backgroundList = ['skin', 'challenge', 'vs', 'persona', 'cafe', 'miku', 'maid', 'coin', 'cover', 'black', 'custom'];
const proxyList = ['direct', 'cn1', 'cn2', 'cn3', 'fr1'];

export {languageList, skinList, soundList, backgroundList, proxyList};

const defaultOptions = {
  gameConfig: {
    judgeOffset: 0,
    visualOffset: 0,
    speed: 5.0,
    resolution: 1,
    noteScale: 1,
    barOpacity: 0.8,
    backgroundDim: 0.7,
    effectVolume: 1,
    showSimLine: true,
    laneEffect: true,
    mirror: false,
    beatNote: true
  },
  language: Quasar.lang.getLocale().replace('-', '_'),
  skin: skinList[0],
  sound: soundList[0],
  bg: backgroundList[0],
  customBg: '',
  autoFullscreen: true,
  upperHidden: false,
  upperHiddenImg: '',
  baseUrl: proxyList[0]
}

const itemName = 'BanG_Player_gameOptions'

export function savePlayerOptions(options) {
  let dOptions = JSON.parse(JSON.stringify(defaultOptions));
  if (options) {
    for (let i in options) dOptions[i] = options[i];
    LocalStorage.set(itemName, dOptions);
  } else throw new Error('ERR_SAVE_OPTIONS_FAILED');
}

export function readPlayerOptions() {
  let options = LocalStorage.getItem(itemName);
  let dOptions = JSON.parse(JSON.stringify(defaultOptions));
  //因为有可能有新添加的属性 在原属性对象里不存在 所以要进行一次映射
  if (options) {
    for (let i in options) dOptions[i] = options[i];
    return dOptions;
  } else {
    LocalStorage.remove(itemName);
    savePlayerOptions(defaultOptions);
    return defaultOptions;
  }
}

export function resetPlayerOptions() {
  LocalStorage.remove(itemName);
  savePlayerOptions(defaultOptions);
  return defaultOptions;
}
