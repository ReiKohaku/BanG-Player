const noteChangeModList = ['none', 'flickParty', 'singleDog', 'allFlick'];
export { noteChangeModList };

export function enableMod(mapContent, modList) {
  if(!modList || modList.length <= 0) return chart;
  let pChart = mapContent;
  let noteChangeModEnabled = false;
  modList.forEach(item => {
    //同一类mod只能有一个生效
    if(item === 'flickParty' && !noteChangeModEnabled) {
      noteChangeModEnabled = true;
      pChart.slides.forEach(item => item.flickend = true)
      pChart.notes.forEach(item => item.type = (item.type === 'slide') ? 'slide' : 'flick');
    } else if(item === 'singleDog' && !noteChangeModEnabled) {
      pChart.slides = [];
      pChart.notes.forEach(item => item.type = 'single');
    } else if(item === 'allFlick' && !noteChangeModEnabled) {
      pChart.slides = [];
      pChart.notes.forEach(item => item.type = 'flick');
    }
  })
  return pChart;
}

export function toModName(modAbbr) {
  if(modAbbr.toLowerCase() === 'fp') return 'flickParty';
  else if(modAbbr.toLowerCase() === 'sd') return 'singleDog';
  else if(modAbbr.toLowerCase() === 'af') return 'allFlick';
  else return modAbbr;
}
