import {LocalStorage} from 'quasar';

const itemName = 'BanG_Player_history';

export function getHistoryList() {
  let history = LocalStorage.getItem(itemName);
  if (!history) history = [];
  return history;
}

export function newHistory(source, type, id, title, artist, author) {
  let history = getHistoryList();
  history.push({time: new Date().getTime(), source, type, id, title, artist, author});
  LocalStorage.set(itemName, history);
}

export function query(source, type, id) {
  let history = getHistoryList();
  let queryList = [];
  for(let i in history) if(history[i].source === source && history[i].type === type && history[i].id === id) queryList.push(history[i]);
  return queryList;
}
