export function bg2bd(notes) {
  //中间变量
  let posA = -1;
  let posB = -1;
  //提供谱面的音符数组
  let bdNotes = [];

  notes.forEach(item => {
    let obj = {
      type: 'Note',
      beat: item.beat[0] + item.beat[2] / item.beat[1]
    }
    switch (item.type) {
      case 'BPM':
        obj.type = 'System';
        obj.cmd = 'BPM';
        obj.bpm = item.value;
        break;
      case 'Single':
        obj.lane = item.lane + 1;
        obj.note = 'Single';
        if(item.tickStack && item.tickStack > -1) {
          obj.note = 'Slide';
          obj.start = true;
          if(posA === -1) {
            posA = item.tickStack;
            obj.pos = 'A';
          }
          else if(posB === -1) {
            posB = item.tickStack;
            obj.pos = 'B';
          }
          else throw new Error('ERR_THREE_SLIDES');
        }
        break;
      case 'Flick':
        obj.lane = item.lane + 1;
        obj.note = 'Single';
        obj.flick = true;
        if(item.tickStack && item.tickStack > -1) {
          obj.note = 'Slide';
          obj.end = true;
          if(posA === item.tickStack) {
            posA = -1;
            obj.pos = 'A';
          }
          else if(posB === item.tickStack) {
            posB = -1;
            obj.pos = 'B';
          }
          else throw new Error('ERR_THREE_SLIDES');
        }
        break;
      case 'SlideTick':
        obj.lane = item.lane + 1;
        obj.note = 'Slide';
        if(!item.tickStack || item.tickStack <= -1) throw new Error('ERR_WRONG_NOTE_ATTRIBUTE');
        if(posA === item.tickStack) obj.pos = 'A';
        else if(posB === item.tickStack) obj.pos = 'B';
        else throw new Error('ERR_THREE_SLIDES');
        break;
      case 'SlideTickEnd':
        obj.lane = item.lane + 1;
        obj.note = 'Slide';
        obj.end = true;
        if(!item.tickStack || item.tickStack <= -1) throw new Error('ERR_WRONG_NOTE_ATTRIBUTE');
        if(posA === item.tickStack) {
          posA = -1;
          obj.pos = 'A';
        }
        else if(posB === item.tickStack) {
          posB = -1;
          obj.pos = 'B';
        }
        else throw new Error('ERR_THREE_SLIDES');
        break;
    }
    bdNotes.push(obj);
  })
  return bdNotes;
}
