/**
 * @param {FileList} file
 * @param {string} type text/dataurl/arraybuffer
 */
export function readFile (file, type = 'text') {
  return new Promise(resolve => {
    const reader = new FileReader();
    reader.onload = e => {
      resolve(e.target.result);
    };
    switch (type) {
      case 'text':
        reader.readAsText(file);
        break;
      case 'dataurl':
        reader.readAsDataURL(file);
        break;
      case 'arraybuffer':
        reader.readAsArrayBuffer(file);
        break;
      default:
        break;
    }
  });
}
