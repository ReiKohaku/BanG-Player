import { Notify } from 'quasar';
export default function (message, caption, icon) {
  Notify.create({ message, caption, icon });
}
