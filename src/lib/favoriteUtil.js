import {LocalStorage} from 'quasar';

const itemName = 'BanG_Player_favoriteList'

export function getList() {
  let favList = LocalStorage.getItem(itemName);
  if (favList) return favList;
  else return [];
}

function saveList(list) {
  LocalStorage.set(itemName, list);
}

export function isInList(source, type, id) {
  let favList = getList();
  if (favList.length <= 0) return false;
  for (let i in favList) {
    if (favList[i].source === source && favList[i].type === type && favList[i].id === id)
      return true;
  }
  return false;
}

export function clear() {
  LocalStorage.remove(itemName);
}

export function remove(source, type, id) {
  let favList = getList();
  if (favList.length <= 0) return false;
  for (let i in favList) {
    if (favList[i].source === source && favList[i].type === type && favList[i].id === id) {
      favList.splice(i, 1);
      saveList(favList);
      return true;
    }
  }
  return false;
}

export function add(source, type, id, title, artist, author) {
  if(isInList(source, type, id)) return false;
  let favList = getList();
  favList.push({ source, type, id, title, artist, author });
  saveList(favList);
  return true;
}
