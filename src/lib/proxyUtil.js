const bdTypeList = {
  direct: 'https://bestdori.com',
  cn1: 'https://bestdori.reikohaku.fun',
  cn2: 'https://bd.reikohaku.fun',
  cn3: 'http://120.92.174.173/bestdori',
  fr1: 'https://bestdori.fr.reikohaku.fun'
}

export default function setProxyUrl(url, type) {
  let targetUrl = bdTypeList[type] ? bdTypeList[type] : bdTypeList.direct;
  return url.replace(bdTypeList.direct, targetUrl);
}
