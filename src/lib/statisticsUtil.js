import axios from 'axios';

export async function newPlay(source, type, id) {
  let { data } = await axios.post(`https://api.bangplayer.reikohaku.fun/play/history/new`, { source, type, id });
  if(!data.status) throw new Error(data.error);
  return data;
}

export async function latestList() {
  let { data } = await axios.post(`https://api.bangplayer.reikohaku.fun/play/history/latest`, { size: 10 });
  if(!data.status) throw new Error(data.error);
  return data;
}

export async function hotList() {
  let { data } = await axios.post(`https://api.bangplayer.reikohaku.fun/play/history/hot`, { size: 10 });
  if(!data.status) throw new Error(data.error);
  return data;
}
