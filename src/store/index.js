import Vue from 'vue'
import Vuex from 'vuex'
import {readPlayerOptions} from "src/lib/playerOptionUtil";

Vue.use(Vuex)

export default function () {
  const Store = new Vuex.Store({
    state: {
      options: readPlayerOptions(),
      gameConfig: null,
      gameLoadConfig: null,
      noteChangeMod: 'none',
      preload: null,
      downloadProgress: 0,
      downloadTotal: null,
      downloadSpeed: '0KB/s',
      loading: false
    },
    getters: {
      options: state => { return state.options; },
      gameConfig: state => { return state.gameConfig; },
      gameLoadConfig: state => { return state.gameLoadConfig; },
      noteChangeMod: state => { return state.noteChangeMod; },
      preload: state => { return state.preload; },
      downloadProgress: state => { return state.downloadProgress; },
      downloadTotal: state => { return state.downloadTotal; },
      downloadSpeed: state => { return state.downloadSpeed; },
      loading: state => { return state.loading }
    },
    mutations: {
      setOptions: (state, payload) => { state.options = payload; },
      setGameConfig: (state, payload) => { state.gameConfig = payload; },
      setGameLoadConfig: (state, payload) => { state.gameLoadConfig = payload; },
      setNoteChangeMod: (state, payload) => { state.noteChangeMod = payload; },
      setPreload: (state, payload) => { state.preload = payload; },
      setDownloadProgress: (state, downloadProgress) => { state.downloadProgress = downloadProgress; },
      setDownloadTotal: (state, downloadTotal) => { state.downloadTotal = downloadTotal; },
      setDownloadSpeed: (state, downloadSpeed) => { state.downloadSpeed = downloadSpeed; },
      resetDownload: (state) => { state.downloadProgress = 0, state.downloadTotal = null, state.downloadSpeed = '0KB/s'; },
      setLoading: (state, loading) => { state.loading = loading; }
    },
    strict: process.env.DEV
  })

  return Store;
}
