[gitee](https://gitee.com/ReiKohaku/BanG-Player)オープンソースコード。

サイトは**@ReiKohaku** ([github](https://github.com/reikohaku) | [gitee](https://gitee.com/ReiKohaku) | [bilibili](https://space.bilibili.com/88633132))から作られています。

シミュレータは**bangbangboom-game** ([github](https://github.com/K024/bangbangboom-game) | [gitee](https://gitee.com/ReiKohaku/bangbangboom-game))。

特に**[Bestdori!](https://bestdori.com)**に感謝します。

もし何か問題や提案があれば、著者に連絡したり、プロジェクトの下でissueを送ることを歓迎します。