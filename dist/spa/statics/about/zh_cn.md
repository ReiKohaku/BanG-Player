开源于[gitee](https://gitee.com/ReiKohaku/BanG-Player)。

网站由**@ReiKohaku** ([github](https://github.com/reikohaku) | [gitee](https://gitee.com/ReiKohaku) | [bilibili](https://space.bilibili.com/88633132))制作。

模拟器使用**bangbangboom-game** ([github](https://github.com/K024/bangbangboom-game) | [gitee](https://gitee.com/ReiKohaku/bangbangboom-game))。

特别感谢**[Bestdori!](https://bestdori.com)**。

如果你有任何问题或建议，欢迎联系作者，或在项目下发送issue。